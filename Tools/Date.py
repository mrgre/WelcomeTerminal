from datetime import datetime
import requests

def GetDateOfServer():
    dateServer = requests.get('http://quotes.rest').headers['date']
    dateServerFormat = '%a, %d %b %Y %X GMT'
    dateServerFormatted = datetime.strptime(dateServer, dateServerFormat)

    exptectedDateFormat = '%Y-%m-%d'
    return dateServerFormatted.strftime(exptectedDateFormat)

if __name__ == "__main__":
    print(GetDateOfServer())