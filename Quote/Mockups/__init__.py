quoteInfoMockup = {
        'quote': 'It is not the mountain we conquer, but ourselves.',
        'length': '49',
        'author': 'Edmund Hillary',
        'tags': ['inspire', 'life', 'mountaineering'],
        'category': 'inspire',
        'date': '2018-08-09',
        'permalink': 'https://theysaidso.com/quote/74qX3QA323z6d1ePUS_GfweF/930975-edmund-hillary-it-is-not-the-mountain-we-conquer-but-ourselves',
        'title': 'Inspiring Quote of the day',
        'background': 'https://theysaidso.com/img/bgs/man_on_the_mountain.jpg',
        'id': '74qX3QA323z6d1ePUS_GfweF',
    }

mockupHttpResponse = '{"success":{"total":1},"contents":{"quotes":[{"quote":"It is not the mountain we conquer, but ourselves.","length":"49","author":"Edmund Hillary","tags":["inspire","life","mountaineering"],"category":"inspire","date":"2018-08-09","permalink":"https://theysaidso.com/quote/74qX3QA323z6d1ePUS_GfweF/930975-edmund-hillary-it-is-not-the-mountain-we-conquer-but-ourselves","title":"Inspiring Quote of the day","background":"https://theysaidso.com/img/bgs/man_on_the_mountain.jpg","id":"74qX3QA323z6d1ePUS_GfweF"}],"copyright":"2017-19 theysaidso.com"}}'