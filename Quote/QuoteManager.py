from Tools import Date
from Quote import QuoteReader
from Quote import QuoteParser
from Quote import QuoteApi

def __FetchAndSaveQuote(category = 'inspire'):
    quoteInfo = QuoteApi.GetQuoteInformation(category)
    if quoteInfo is not None:
        QuoteReader.__SaveQuoteInformation(quoteInfo)
    return quoteInfo

def GetQuote(category = 'inspire'):
    quoteInfo = QuoteReader.__ReadQuoteInformation()

    # Missing QuoteOfDay.json file
    if (quoteInfo is None):
        quoteInfo = __FetchAndSaveQuote(category)

    if quoteInfo is not None:
        publicationDate = QuoteParser.__GetDateOfQuote(quoteInfo)
        today = Date.GetDateOfServer()
        # Outdated quote
        if (publicationDate != today):
            __FetchAndSaveQuote(category)

    return quoteInfo


if __name__ == "__main__":
    print(GetQuote())
