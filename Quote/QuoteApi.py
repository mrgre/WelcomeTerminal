from Quote import QuoteParser
import requests

def __GetHttpResponseQuote(category = 'inspire'):
    url = f'http://quotes.rest/qod.json?category={category}'        
    httpResponse = requests.get(url)
    if httpResponse.status_code == 200:
        return httpResponse.text
    else:
        return None

def GetQuoteInformation(category = 'inspire'):
    httpResponse = __GetHttpResponseQuote(category)
    return QuoteParser.__GetQuoteInformationFromHttpResponse(httpResponse)
    
if __name__ == "__main__":
    print(GetQuoteInformation())